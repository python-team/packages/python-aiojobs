-r ci.txt
-r doc.txt
mypy==1.11.0
pre-commit==3.7.1
pytest-sugar==1.0.0
